import TestUtils from '@tarojs/test-utils-react'
import Button from './index'
const testUtils = new TestUtils();
describe('ui测试', () => {
  it('测试children是否渲染', async () => {
    // React跟Vue相同用法
    await testUtils.mount(Button, {
      props: {
        children: 'button',
      },
    });
    expect(testUtils.queries.queryByText("button")).toBeTruthy();
  })
})