import { ButtonType } from './button';
import React from 'react';

const baseStyles = {
  borderRadius: 'var(--button-border-radius)',
}

const conditionStyles: Record<ButtonType, React.CSSProperties> = {
  "default": {
    backgroundColor: 'var(--button-default-background-color)',
    color: 'var(--button-default-color)',
    border: '1px solid var(--button-default-border-color)'
  },
  "primary": {
    backgroundColor: 'var(--button-primary-background-color)',
    color: 'var(--button-primary-color)',
    border: '1px solid var(--button-primary-border-color)'
  },
  "warning": {
    backgroundColor: 'var(--button-warning-background-color)',
    color: 'var(--button-warning-color)',
    border: '1px solid var(--button-warning-border-color)'
  },
  "danger": {
    backgroundColor: 'var(--button-danger-background-color)',
    color: 'var(--button-danger-color)',
    border: '1px solid var(--button-danger-border-color)'
  },
  "info": {
    backgroundColor: 'var(--button-info-background-color)',
    color: 'var(--button-info-color)',
    border: '1px solid var(--button-info-border-color)'
  },
}

const baseTextButtonStyle: React.CSSProperties = {
  display: "inline",
  backgroundColor: 'transparent',
  margin: 0,
  padding: 0,
  lineHeight: "inherit"
}

const textButtonStyles: Record<ButtonType, React.CSSProperties> = {
  "default": {
    ...baseTextButtonStyle,
    color: 'var(--button-text-default-color)',
  },
  "primary": {
    ...baseTextButtonStyle,
    color: 'var(--button-text-primary-color)',
  },
  "warning": {
    ...baseTextButtonStyle,
    color: 'var(--button-text-warning-color)',
  },
  "danger": {
    ...baseTextButtonStyle,
    color: 'var(--button-text-danger-color)',
  },
  "info": {
    ...baseTextButtonStyle,
    color: 'var(--button-text-info-color)',
  },
}

const outlinedButtonStyles: Record<ButtonType, React.CSSProperties> = {
  "default": {
    color: 'var(--button-text-default-color)',
  },
  "primary": {
    color: 'var(--button-text-primary-color)',
  },
  "warning": {
    color: 'var(--button-text-warning-color)',
  },
  "danger": {
    color: 'var(--button-text-danger-color)',
  },
  "info": {
    color: 'var(--button-text-info-color)',
  },
}

const disabledBaseStyle: React.CSSProperties = {
  backgroundColor: 'var(--button-disabled-background-color)',
  color: 'var(--button-disabled-color)',
  border: "1px solid var(--button-disabled-background-color)",
};

const disabledTextStyle: React.CSSProperties = {
  color: "var(--button-text-disabled-color)",
  backgroundColor: 'transparent',
  border: "none",
};

/** 获取style*/
export const getStyle = (type: ButtonType, disabled: boolean, round: boolean, text: boolean, outlined: boolean): React.CSSProperties => {
  let result: React.CSSProperties = { ...baseStyles };

  result = {
    ...result,
    ...(text ? textButtonStyles[type] : conditionStyles[type]),
  };

  if (outlined) {
    result = {
      ...result,
      ...outlinedButtonStyles[type],
      backgroundColor: "var(--button-outlined-background-color)"
    };
  }
  if (disabled) {
    result = {
      ...result,
      ...(text ? disabledTextStyle : disabledBaseStyle),
    };
  }
  if (round) {
    result = {
      ...result,
      borderRadius: "var(--border-radius-max)",
    };
  }
  return result;
}
