export type ButtonType = 'default' | 'primary' | 'info' | 'warning' | 'danger';
export type ButtonVariantType = 'text' | 'outlined'
// button.d.ts
export interface ButtonProps {
  /** 类别*/
  type?: ButtonType;
  /** 按钮样式 text 还是 outlined*/
  variant?: ButtonVariantType
  /** 样式*/
  style?: React.CSSProperties;
  /** 点击事件*/
  onClick?: () => void;
  /** 是否禁用*/
  disabled?: boolean;
  /** 是否是圆形按钮*/
  round?: boolean;
  /** 按钮内容*/
  children?: React.ReactNode;
  /** Any other props */
  [key: string]: any;
}

declare const Button: React.FunctionComponent<ButtonProps>;

export default Button;
