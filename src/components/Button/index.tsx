import React, { useCallback, useMemo } from 'react';
import { Button, View } from '@tarojs/components';
import { ButtonProps } from './button.d';
import { getStyle } from './button.utils'
import classNames from 'classnames';
import './../../style/var.scss'
import './index.scss'

export default function Index(props: ButtonProps) {
  const {
    type = 'default',
    style = {},
    children = 'button',
    onClick = () => { },
    disabled = false,
    round = false,
    variant = null
  } = props;

  /** 按钮是否为文字按钮*/
  const text = useMemo(() => {
    return variant === "text"
  }, [variant])

  /** 按钮是否为轮廓按钮*/
  const outlined = useMemo(() => {
    return variant === "outlined"
  }, [variant])

  /** 框样式*/
  const frameSize: React.CSSProperties = useMemo(() => {
    let result = {}
    if (style.hasOwnProperty('height')) {
      result['height'] = style['height']
    }
    if (style.hasOwnProperty('width')) {
      result['width'] = style['width']
    }
    return result
  }, [style])

  /** 类名*/
  const resultClass = useMemo(() => {
    return classNames({
      'md-button': true,
      'md-button__text': text,
      'md-button__outlined': outlined
    });
  }, [text, outlined])

  /** 按钮样式*/
  const resultStyle = useMemo(() => {
    return {
      ...getStyle(type, disabled, round, text, outlined),
      ...style,
    }
  }, [type, style, disabled, round, text, outlined])

  /** 触发点击*/
  const handelClick = useCallback(() => {
    if (disabled) {
      return
    }
    onClick()
  }, [disabled, onClick])

  return <View className={resultClass} style={frameSize}>
    <Button
      {...props}
      className='md-button__container'
      hoverClass='md-button__action'
      type='primary'
      onClick={handelClick}
      disabled={disabled}
      style={resultStyle}
    >
      {children}
    </Button>
  </View>
}