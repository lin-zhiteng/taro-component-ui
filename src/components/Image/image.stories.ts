import type { Meta, StoryObj } from '@storybook/react';
import React from 'react'
import Image from './index';

// More on how to set up stories at: https://storybook.js.org/docs/writing-stories#default-export
const meta = {
  title: 'Example/Image',
  component: Image,
  parameters: {

  },
} satisfies Meta<typeof Image>;

export default meta;
type Story = StoryObj<typeof meta>;

// More on writing stories with args: https://storybook.js.org/docs/writing-stories/args
export const Primary: Story = {
  args: {
    src:"https://img.yzcdn.cn/vant/cat.jpeg"
  },
};

