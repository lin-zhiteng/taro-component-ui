import React, { useCallback } from 'react';
import { Image } from "@tarojs/components"
import { ImageProps } from './image.d'
import './../../style/var.scss'
import Taro from '@tarojs/taro';

export default function Index(props: ImageProps) {

  const {
    src,
    mode = "scaleToFill",
    style = {},
    onClick = () => { },
    preview = false
  } = props

  /** 预览图片*/
  const previewImage = useCallback(() => {
    Taro.previewImage({
      current: src,
      urls: [src]
    });
  }, [src]);

  /** 触发点击事件*/
  const handleClick = useCallback(() => {    
    onClick()
    if (preview) {
      previewImage()
    }
  }, [onClick, previewImage])

  return <>
    <Image
      {...(props as any)}
      mode={mode}
      src={src}
      style={style}
      onClick={handleClick}
    >
    </Image>
  </>
}