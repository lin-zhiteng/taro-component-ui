export type ImageModeType = 'scaleToFill' | 'aspectFit' | 'aspectFill' | 'widthFix' | 'heightFix' | 'top' | 'bottom' | 'center' | 'left' | 'right' | 'top left' | 'top right' | 'bottom left' | 'bottom right'

export interface ImageProps {
  /** 资源地址*/
  src: string;
  /** 图片加载的模式*/
  mode?: ImageModeType;
  /** 样式 */
  style?: React.CSSProperties;
  preview?: boolean;
  /** 点击事件*/
  onClick?: () => void
  /** Any other props */
  [key: string]: any;
}

declare const Image: React.FunctionComponent<ImageProps>;

export default Image;
