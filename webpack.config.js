const path = require("path")
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

/** @type {import('webpack'.Configuration)} */
module.exports = {
  entry: './src/index.ts',
  output: {
    filename: 'index.js',
    path: path.resolve(__dirname, 'componentsDist'),
    clean: true,
    library: {
      type: 'umd',
    },
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.jsx', '.json'],
  },
  mode: "production",

  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: {
          loader: "ts-loader",
          //不做ts检查只是转译
          options: {
            transpileOnly: true
          }
        },
        exclude: [
          path.resolve(__dirname, 'node_modules'),
          path.resolve(__dirname, 'src/pages')
        ],
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          // 提取CSS到单独的文件
          MiniCssExtractPlugin.loader,
          // 将 CSS 转化成 CommonJS 模块
          'css-loader',
          // 将 Sass 编译成 CSS
          'sass-loader',
        ],
      },
    ]
  },

  plugins: [
    // 生成css文件
    new MiniCssExtractPlugin({
      filename: '[name].css',
      chunkFilename: '[id].css',
    }),
  ],

  externals: {
    react: 'react',
    'react-dom': 'react-dom',
    '@tarojs/components': '@tarojs/components',
    '@tarojs/runtime': '@tarojs/runtime',
    '@tarojs/taro': '@tarojs/taro',
    '@tarojs/react': '@tarojs/react',
  },
}